#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  geomfinder.py
#  
#  Copyright 2015 Gabriel Nuñez-Vivanco <ganunez@utalca.cl>, Alejandro Valdés-Jiménez <avaldes@utalca.cl> 
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  

import threading
from pdbPocket import PDBPOCKET
from Bio.PDB import PDBIO
from Bio.PDB.PDBParser import PDBParser
import numpy as np
import os
from tsp_solver.greedy import solve_tsp
import time
import argparse
import ujson as json
from Bio.PDB import NeighborSearch, Selection
from Bio.PDB.Vector import Vector
from numpy import array

class Pockets(threading.Thread):
  def __init__(self, args, filename, DIRDATA, letter):
    threading.Thread.__init__(self)
    
    # rutas donde dejar datos.
    self.pathInput1 = DIRDATA + "/pdbs/" + filename
    self.pathInput2 = DIRDATA + "/sr/" + filename
    
    # parámetros línea de comandos.
    self.args = args
    self.filename = filename
    self.chain = None
    self.records = None
    
    if 'empty' in letter:
      self.letter = ' '
    else:
      self.letter = letter
    
    return
  
  def run(self):
    # lee estructura del PDB.
    parser = PDBParser()
    structure = parser.get_structure(self.filename, self.filename)
    model = structure[0]
    self.chain = model[self.letter]
    self.records, self.ns, self.bind_sites = self.getRecordBounding(self.args.gridPCT)
    return
  
  def getCoordRecords(self, dist):
    Coords = []
    x = self.records[0][0]
    while(x < self.records[0][1]):
      y = self.records[1][0]
      while(y < self.records[1][1]):
        z = self.records[2][0]
        while(z < self.records[2][1]):
          Coords.append((x,y,z))
          z = z + dist
        y = y + dist
      x = x + dist
    return Coords
  
  def getRecordBounding(self, dist):
    Xatom = []
    Yatom = []
    Zatom = []
    atom_list = []
    dummy_coords = []
    bind_site = []

    i=1
    j=1
    
    # revisa cada residuo de la cadena.
    for residue in self.chain:
      hetflag, resseq, icode = residue.get_id()
      # verifica que no sea un ligando.
      if hetflag ==" ":
        # revisa cada atomo del residuo.
        for atom in residue:
          # considerar átomos de la cadena lateral y obtener sus coordenadas.
          if (atom.get_name()!='N' and atom.get_name()!='CA' and atom.get_name()!='C' and atom.get_name()!='O' and atom.get_name()!='H'):
            Xatom.append(atom.get_coord()[0])
            Yatom.append(atom.get_coord()[1])
            Zatom.append(atom.get_coord()[2])
            atom_list.append(atom)
    
    # obtiene clase con átomos de las cadenas laterales para realizar búsquedas más adelante.
    ns = NeighborSearch(atom_list)

    # revisa nuevamente cada residuo de la cadena.
    for residue in self.chain:
      hetflag, resseq, icode = residue.get_id()
      if hetflag ==" ":
        try:
          # busca en las cadenas laterales elementos cercanos al centro geométrico del residuo.
          set_residues= ns.search(coord(residue,1), dist, 'R')
        except:
          return None, None, None
        
        # se encontraron?
        if len(set_residues)>1:
          coor1 = coord(residue,1)
          for r2 in set_residues:
              coor2  = coord(r2,1)
              # calcula punto medio.
              cg = (coor1[0] + coor2[0]/2, coor1[1] + coor2[1]/2, coor1[2] + coor2[2]/2)
              dummy_coords.append(cg)

          cg=np.sum(coord(r,1) for r in set_residues) / len(set_residues)
          cg = array(cg)
        else:
          dummy_coords.append(coord(residue,1))
      
      # es un ligando (HETATM)
      elif hetflag !=" " and hetflag !="W":
        center_atoms = Selection.unfold_entities(residue, 'A')
        nearby_residues = {res for center_atom in center_atoms
                   for res in ns.search(center_atom.coord, 6, 'R')}
        bind = hetflag + ":"
        for res in nearby_residues:
          bind = bind + res.get_resname() + str(res.get_id()[1]) + ":"
        bind_site.append(bind)

    return dummy_coords, ns, bind_site
    
  '''
    retorna lista de residuos.
  '''
  def getResidues(self):
    tmpList = []
    for residue in self.chain:
      id = residue.id
      if id[0] == ' ':
        tmpList.append(residue)
    return tmpList

'''
'''
class finding(threading.Thread):
  def __init__(self, umbralMax, ns, atm, atom, umbralMin, pathInput1, pathInput2, bind_sites):
    threading.Thread.__init__(self)
    self.umbralMax = umbralMax
    self.ns = ns
    self.atm = atm
    self.atom = atom
    self.umbralMin = umbralMin
    self.pathInput1 = pathInput1
    self.pathInput2 = pathInput2
    self.bind_sites = bind_sites
  
  def run(self):
    self.l = self.line()
    return

  def line(self):
    temp_pocket = []
    array1 = []
    d = self.umbralMin
    
    while d <= self.umbralMax:
      pocketlist1= self.ns.search(array(self.atom), d, 'R')
      
      if len(pocketlist1) > 3:
        spocketn = ":"
        pocketlist1_number = []
        pocket_compare = []
        for residue in pocketlist1:
          pocketlist1_number.append(residue.get_id()[1])
          spocketn = spocketn + str(residue.get_id()[1]) +":"
          pocket_compare.append(residue.get_resname() + str(residue.get_id()[1]))
        
        pocketlist1_number = sorted(pocketlist1_number)
        uflag =  unique(spocketn,temp_pocket)
         
        if uflag == True:
          temp_pocket.append(spocketn)
          
          pdb_maker(pocketlist1,"Atom"+str(self.atm)+":"+str(d), self.pathInput1, self.pathInput2)
          finger_dist,scoreASS,finger_nbonded,matrix2,matrix3 = getFeatures(pocketlist1)
          path = solve_tsp(matrix3)
          finger_tsp = getTSP(path,matrix2,matrix3)
          site1 = "PDB1_Pocket", self.atm, finger_dist, finger_nbonded,scoreASS, finger_tsp, d, "Atom",len(pocketlist1),share(self.bind_sites, pocket_compare)
         
          array1.append(site1)
         
        del pocketlist1[:]
        del pocketlist1_number
          
      d = d + 1
    return array1
 
def pdb_maker(pocketlist1, name, path, pathsr):
  namesr = path + "/" + name + ".pdb.txt"
  pocket_view = open(namesr,"w")
  line = ''

  pp = PDBPOCKET(path + "/" + name)

  for residue in pocketlist1:
    r = pp.createResidue(residue.get_id(),residue.get_resname(), 0)
    pp.addResidueToChain(r)
    if len(line) > 0:
      line = line +","
    line = line + residue.get_resname() + str(residue.get_id()[1])
  
    for atom in residue:
      a = pp.createAtom(atom.get_name(),atom.get_coord(), atom.get_bfactor(), atom.get_occupancy(), ' ',atom.get_fullname(), atom.get_serial_number(), atom.element)
      pp.addAtomToResidue(r, a)

  pp.writePDBFile()
  pocket_view.close()

# retorna centro geométrico del grupo R.
def coord(residue, i):
  atomlist = []
  
  if i == 1:
    if residue.get_resname() == 'GLY':
      # FIXME: bucasr CB, si no existe, utiliza CA.
      return (residue['CA'].get_coord())
    else:
      for atom in residue:
        # filtra por átomos del grupo R
        if (atom.get_name()!='N' and atom.get_name()!='CA' and atom.get_name()!='C' and atom.get_name()!='O' and atom.get_name()!='H'):
          atomlist.append(atom)
  
  if i == 2:
    for atom in residue:
      atomlist.append(atom)

  if len(atomlist)==0:
    cg = 0
  else:
    # calcula centro geométrico.
    cg=np.sum(atom.get_coord() for atom in atomlist) / len(atomlist)

  return cg

def nbonded(res):
    ch = { 'ASP': -1.162, 'GLU': -1.163, 'LYS': -1.074, 'ARG': -0.921, 'HIS': -1.215, 'PRO': -1.236, 'GLY': -1.364, 'ALA': -1.404, 'VAL': -1.254, 'ILE': -1.189, 'LEU': -1.315, 'SER': -1.297, 'THR': -1.252, 'MET': -1.303, 'CYS': -1.365, 'GLN': -1.116, 'ASN': -1.178, 'PHE': -1.135, 'TRP': -1.030, 'TYR': -1.030}
    return ch[res.get_resname()]

def hydrop(res):
    ch = { 'ASP': 0.66, 'GLU': 0.67, 'LYS': 1.64, 'ARG': 0.85, 'HIS': 0.87, 'PRO': 2.77, 'GLY': 0.1, 'ALA': 0.87, 'VAL': 1.87, 'ILE': 3.15, 'LEU': 2.17, 'SER': 0.07, 'THR': 0.07, 'MET': 1.67, 'CYS': 1.52, 'GLN': 0, 'ASN': 0.09, 'PHE': 2.87, 'TRP': 3.77, 'TYR': 2.67}
    return ch[res.get_resname()]

# http://www.sigmaaldrich.com/life-science/metabolomics/learning-center/amino-acid-reference-chart.html#prop
def weight(res):
    wm = { 'ASP': 133.11, 'GLU': 147.13, 'LYS': 146.19, 'ARG': 174.20, 'HIS': 155.16, 'PRO': 115.13, 'GLY': 75.05, 'ALA': 89.10, 'VAL': 117.15, 'ILE': 131.18, 'LEU': 131.18, 'SER': 105.09, 'THR': 119.12, 'MET': 149.21, 'CYS': 121.16, 'GLN': 146.15, 'ASN': 132.12, 'PHE': 165.19, 'TRP': 204.23, 'TYR': 181.19}
    return wm[res.get_resname()]

def getTSP(path,matrix2,matrix3):
  i = 0
  j = 0
  u = 0
  tsp = 0
  finger_tsp=""
  while u < len(path)-1:
    while i < len(path):
      while j < len(path):
        if path[u] == j and path[u+1] == i:
          aa1=matrix2[j]
          aa2=matrix2[i]
          ab2= letter_aa(aa1[:3]) + letter_aa(aa2[:3])
          
          absort2 = ''.join(sorted(ab2))
            
          finger_tsp= finger_tsp + absort2 + str(round(matrix3[i][j])) + ":"
        j = j+1
      i= i +1
      j = 0
    i = 0
    u = u + 1          

  return finger_tsp

def getFeatures(pocketlist1):
  scoredist1 = 0
  scorenbonded1 = 0
  scoreASS = ""
  ndist = 0
  finger_dist=""
  finger_angle=""
  finger_nbonded=0
  i=0
  j=0
  matrix2 = []
  x=0
  flagx = 0
  
  matrix3 = [[0 for x in xrange(len(pocketlist1))] for x in xrange(len(pocketlist1))] 

  for res in pocketlist1:
    finger_nbonded = finger_nbonded + (nbonded(res) +1)* weight(res)
    # Componentes, casi alineamiento
    scoreASS = scoreASS + letter_aa(res.get_resname()[:3])+":"
         
    coor1 = coord(res,1)
    resId = str(res.get_id())
  
    for res2 in pocketlist1:
      coor2 = coord(res2,1)
      try:
        beta_dist=0
        beta_dist = np.linalg.norm(coor1-coor2)
      except KeyError:
        pass

      if j < len(pocketlist1)-1:
        matrix3[j][i] = round(beta_dist, 4)
        j = j + 1
      else:
        matrix2.append(res.get_resname()+resId[6]+resId[7]+resId[8])
        matrix3[j][i] = round(beta_dist, 4)
        j = 0
        i = i + 1
      if(res.get_id() != res2.get_id()):
        ab= letter_aa(res.get_resname()[:3]) + letter_aa(res2.get_resname()[:3])
        absort = ''.join(sorted(ab))
        finger_dist= finger_dist + absort+str(round(beta_dist)) + ":"
      
  return finger_dist,scoreASS,finger_nbonded,matrix2,matrix3

def is_pattern(list_residues,umbralMin):
  for r1 in list_residues:
    coor1 = coord(r1,1)

    for r2 in list_residues:
      if(r1.get_id() != r2.get_id()):
        coor2 = coord(r2,1)
        beta_d = np.linalg.norm(coor1-coor2)

        if beta_d < umbralMin:
                 
          return False

  return True

def unique(list_residues, sets_lists):
  if list_residues in sets_lists:
    return (False)
  else:
    return (True)

def are_min_max(scoref_num,mini,maxi):
  cont = 0
  similar = 0
  pairs= scoref_num.split(":")
  
  for pair in pairs:
    if pair != "":
      if float(pair)<= mini or float(pair) >= maxi:
        return True

  return False

def inv(pair):
  temp = ""
  i = 1
  temp = temp + pair[len(pair)-1]
  while i < len(pair)-1:
    temp = temp + pair[i]
    i = i + 1
  temp = temp + pair[0]

  return temp

def scoreNbE(score1, score2):
  num = min(abs(score1),abs(score2))
  den = max(abs(score1),abs(score2))
  scoredistf = float(num)/float(den)
  
  if (score1 > 0 and score2) < 0 or (score1 < 0 and score2 > 0):
    scoredistf = 1 - scoredistf
  
  return (scoredistf)

def share(score1,score2):
  sims = []
  similar = ""
  ex=""
  
  for bind in score1:
    similar = ""
    Sscore1 = list(bind.split(":"))
    drug = Sscore1[0]
    Sscore1.remove(drug)
    Sscore1.pop()
    Sscore1 = set(Sscore1)
    
    Sscore2 = set(score2)
    shared = Sscore1.intersection(Sscore2)

    if len(shared) == 0:
      pass
    else:
      for res in shared:
        similar = similar + res +":"
      similar = drug.split("_")[1] +"-"+ similar
      sims.append(similar)

  if len(sims) > 1:
    ex = ""
    for s in sims:
      ex = ex + s + " | "
  elif len(sims) == 1:
    ex = sims[0]
  return ex  

def scoreTd(score1,score2):
  similar = 0
  res = 0
  
  Sscore1 = score1.split(":")
  Sscore2 = score2.split(":")

  Sscore1.pop()
  Sscore2.pop()
  
  l1= len(Sscore1)
  l2= len(Sscore2)
  
  for s1 in Sscore1:
    sn1 = float(s1[2:])
    
    for s2 in Sscore2:
      sn2 = float(s2[2:])
      
      # son las mismas letras?
      if (s1[:2] == s2[:2]) and (abs(sn1-sn2)<=2):
        ###print s1[:2], s2[:2]
        similar = similar + 1
        Sscore2.remove(s2)
        break
  
  res =  similar/float(max(l1,l2))
  return res
   
def scoreT(score1,score2):
  similar = 0
  
  Sscore1 = score1.split(":")
  Sscore2 = score2.split(":")

  l1= len(Sscore1)
  l2= len(Sscore2)

  for s1 in Sscore1:
    for s2 in Sscore2:
      if s1 == s2:
        similar = similar +1
        Sscore2.remove(s2)
        break

  return similar/float(max(l1,l2))    
  
'''
  define las opciones de la linea de comandos.
'''
def readOptions ():
  parser = argparse.ArgumentParser(description='Busca similitud de estructuras entredos proteínas.')
  
  parser.add_argument('PDB_input', metavar='protein1', help='Primera proteína')
  parser.add_argument('PDB_input2', metavar='protein2', help='Segunda proteína')
  parser.add_argument('-chainP1', action='store', metavar='chain', dest='chainP1', default="A", help='chain protein1 (default: A)')
  parser.add_argument('-chainP2', action='store', metavar='chain', dest='chainP2', default="A", help='chain protein2 (default: A)')
  parser.add_argument('-thresholdMin', action='store', metavar='float', dest='umbralMin', type=float, default=4, help='umbral mínimo (default: 5 Angstroms)')
  parser.add_argument('-thresholdMax', action='store', metavar='float', dest='umbralMax', type=float, default=5, help='umbral máximo (default: 7 Angstroms)')
  parser.add_argument('-filterMax', action='store', metavar='integer', dest='filterMax', type=float, default=50, help='umbral máximo (default: 80)')
  parser.add_argument('-nbondedPCT', action='store', metavar='float', dest='nbondedPCT', type=float, default=25, help='nbonded percentage (default 20)')
  parser.add_argument('-distancePCT', action='store', metavar='float', dest='distancePCT', type=float, default=25, help='distance percentage (default 20)')
  parser.add_argument('-tspPCT', action='store', metavar='float', dest='tspPCT', type=float, default=25, help='tsp percentage (default 20)')
  parser.add_argument('-assPCT', action='store', metavar='float', dest='assPCT', type=float, default=25, help='ASS percentage (default 20)')
  parser.add_argument('-gridPCT', action='store', metavar='float', dest='gridPCT', type=float, default=7, help='Size of spacing on grid (default 3)')
  
  args = parser.parse_args()
  return args

def letter_aa(temp):# A NEW CLASSIFICATION OF THE AMINO ACID SIDE CHAINS BASED ON DOUBLET ACCEPTOR ENERGY LEVELS
  ch = { 'ASP': 'D', 'GLU': 'E', 'LYS': 'K', 'ARG': 'R', 'HIS': 'H', 'PRO': 'P', 'GLY': 'G', 'ALA': 'A', 'VAL': 'V', 'ILE': 'I', 'LEU': 'L', 'SER': 'S', 'THR': 'T', 'MET': 'M', 'CYS': 'C', 'GLN': 'Q', 'ASN': 'N', 'PHE': 'P', 'TRP': 'W', 'TYR': 'Y'}
  return ch[temp]

def letter_aa_group(temp):# A NEW CLASSIFICATION OF THE AMINO ACID SIDE CHAINS BASED ON DOUBLET ACCEPTOR ENERGY LEVELS
  ch = { 'ASP': 'D', 'GLU': 'D', 'LYS': 'F', 'ARG': 'F', 'HIS': 'F', 'PRO': 'H', 'GLY': 'A', 'ALA': 'A', 'VAL': 'A', 'ILE': 'A', 'LEU': 'A', 'SER': 'C', 'THR': 'C', 'MET': 'G', 'CYS': 'G', 'GLN': 'E', 'ASN': 'E', 'PHE': 'B', 'TRP': 'B', 'TYR': 'B'}
  return ch[temp]

'''

### line_profiler: agregar decorator @profile en las funciones a analizar.

// genera archivo geomfinder.py.lprof
$ kernprof -l geomfinder.py 1EXP.pdb 2O3P.pdb  

// muestra resultados
$ python -m line_profiler geomfinder.py.lprof

// redirige resultados:
$ python -m line_profiler geomfinder.py.lprof > lines.txt

### 
// no necesita decorator @profile

// genera archivo geomfinder.pstats 
$ python -Wignore -m cProfile -o geomfinder.pstats geomfinder.py 1EXP.pdb 2O3P.pdb 

// visualuzar estadísticas
$ runsnake geomfinder.pstats

// genera grafo
$ gprof2dot -f pstats -c pink geomfinder.pstats | dot -Tpng -o geomfinder.pstats.png

###
// agregar decorator @profile en las funciones a analizar.

// estadisticas de uso de memoria
python -Wignore -m memory_profiler -o geomfinder-memory.pstats geomfinder.py 1EXP.pdb 2O3P.pdb 

'''

def main():
  # recupera parámetros de la línea de comandos.
  args = readOptions()
  sitesA=[]
  sitesB=[]
  
  # archivos de entrada.
  PDB_input = args.PDB_input
  PDB_input2 = args.PDB_input2
  
  # directorio de trabajo y resultados único por solicitud.
  DIRDATA = "results/" + str(time.time())
  os.system("mkdir -p " + DIRDATA)
  os.system("mkdir -p " + DIRDATA + "/dats")
  os.system("mkdir -p " + DIRDATA + "/pdbs/" + PDB_input)
  os.system("mkdir -p " + DIRDATA + "/pdbs/" + PDB_input2)
  os.system("cp atmtypenumbers " + DIRDATA + "/pdbs/" + PDB_input)
  os.system("cp atmtypenumbers " + DIRDATA + "/pdbs/" + PDB_input2)

  # instancias de cada proteina, busca pockets.
  p1 = Pockets(args, args.PDB_input, DIRDATA, args.chainP1)
  p1.start()
  
  p2 = Pockets(args, args.PDB_input2, DIRDATA, args.chainP2)
  p2.start()

  # espera por el término de ambas hebras.
  p1.join()
  p2.join()

  if p1.records:
    pass
  else:
    # sin registros
    # DIRDATA
    print None
    return 0
    
  if p2.records:
    pass
  else:
    # sin registros
    # DIRDATA
    print None
    return 0

  ################ Pockets protein A ############################3
  hiloA = 0
  threads1 = []
  array1 = []
  
  for atom in p1.records:
    procA = finding(args.umbralMax, p1.ns, hiloA, atom, args.umbralMin, p1.pathInput1, p1.pathInput2, p1.bind_sites)
    procA.start()
    hiloA = hiloA + 1
    threads1.append(procA)
  
  for proc in threads1:
    proc.join()
      
  for proc in threads1:
    if len(proc.l) != 0 and unique(proc.l[0][4], sitesA)==True:
      array1.append(proc.l)
      sitesA.append(proc.l[0][4])

  ################ Pockets protein B ##########################
  hiloB = 0
  threads2 = []
  array2 = []
  
  for atom in p2.records:
    procB = finding(args.umbralMax, p2.ns, hiloB, atom, args.umbralMin, p2.pathInput1, p2.pathInput2, p2.bind_sites)
    procB.start()
    hiloB = hiloB + 1
    threads2.append(procB)
  
  for proc in threads2:
    proc.join()
      
  for proc in threads2:
    if len(proc.l) != 0 and unique(proc.l[0][4],sitesB)==True:
      array2.append(proc.l)
      sitesB.append(proc.l[0][4])
  
  ################################################
  ################################################
  
  if len(array1)>0 and len(array2)>0:
    # primera línea en cada archivo de datos
    i = 0
    
    # PARAMS JSON
    jsonParams = {}
    jsonParams["params"] = [{"protein1": args.PDB_input.split("/")[len(args.PDB_input.split("/"))-1][:-4], 
                            "protein2": args.PDB_input2.split("/")[len(args.PDB_input2.split("/"))-1][:-4], 
                            "umbralMin": args.umbralMin, 
                            "umbralMax": args.umbralMax, 
                            "filterMax": args.filterMax, 
                            "nbondedPCT": args.nbondedPCT, 
                            "distancePCT": args.distancePCT, 
                            "tspPCT": args.tspPCT, 
                            "assPCT": args.assPCT, 
                            "chainP1":args.chainP1, 
                            "chainP2": args.chainP2, 
                            "gridPCT": args.gridPCT}]

    with open(DIRDATA + "/dats/params.json", "w") as outfile:
      json.dump(jsonParams, outfile)
    
    # SCORE JSON
    jsonTable = {}
    allScores = []
    pathInput22 = DIRDATA + "/sr/" + args.PDB_input2
    
    # compara los pockets encontrados en cada proteína.
    i = 0
    for text in array1:
      j = 0
      text = text[0]
      for text2 in array2:
        text2 = text2[0]
        scoredistf = scoreTd(text[2],text2[2])
        scorenbondedf = scoreNbE(text[3],text2[3])
        scoreASSf = scoreT(text[4],text2[4])
        scoretspf = scoreTd(text[5],text2[5])
        scoref = scoredistf*(float(args.distancePCT)*0.01) + scorenbondedf*(float(args.nbondedPCT)*0.01) + scoreASSf*(float(args.assPCT)*0.01) + scoretspf*(float(args.tspPCT)*0.01)

        if scoref >= float(args.filterMax*0.01):          
          theScore = {"pocket1": text[7] + str(text[1])+":"+str(text[6]),
          "pocket2": text2[7] + str(text2[1])+":"+str(text2[6]), "scoref": round(scoref*100,1), 
          "scoredistf": round(scoredistf*100,1), "scorenbondedf": round(scorenbondedf*100,1), 
          "scoreASSf": round(scoreASSf*100,1), "scoretspf": round(scoretspf*100,1), "resP1": str(text[8]), 
          "resP2": str(text2[8]), "url": "<a href=\"javascript:show3D('" + args.chainP1 + "','" + args.chainP2 + 
          "','" + args.PDB_input.split("/")[len(args.PDB_input.split("/"))-1] + "','" + 
          args.PDB_input2.split("/")[len(args.PDB_input2.split("/"))-1] + "','" + "pdbs/" + PDB_input + "/" +
          text[7] + str(text[1])+":"+str(text[6]) + ".pdb','" +  "pdbs/" + PDB_input2 + "/" + text2[7] + 
          str(text2[1])+":"+str(text2[6]) + ".pdb');\"><img src='imgs/atom.png'></a>","Bsite1": text[9], "Bsite2":text2[9]}
          allScores.append(theScore)
        j = j + 1
      i = i + 1
    
    jsonTable["scores"] = allScores
    with open(DIRDATA + "/dats/scores.json", "w") as outfile:
      json.dump(jsonTable, outfile)
      
  else:
    os.system("rm -rf " + DIRDATA)
    DIRDATA = None

  # imprime el nombre del directorio donde están los resultados o None si no hubieron resultados.
  print DIRDATA
  return 0

'''
  main
'''  
if __name__ == "__main__":
  main()

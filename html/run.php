<?php
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  untitled.py
#  
#  Copyright 2015 gabriel Nunez <gabriel@ganunez>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

require_once 'lib/common.php';
head_page();
open_body_page();

?>
<!--<div class="container theme-showcase" role="main">-->
<div class="container-fluid">
  <div class="page-header">
    <h2><?php echo $GLOBALS["Name"]; ?>: <?php echo $GLOBALS["Title"]; ?> </h2>
  </div>

  <!--<form onsubmit="return validateProteinsIDs()" name="formChains" method="POST" enctype="multipart/form-data" class="form-horizontal">-->
  
  <!-- row -->
  <div class="row">
    
    <!-- column -->
    <div class="col-lg-12">
      
      <div class="panel panel-default">
        
        <div class="panel-heading">
          <span class="panel-title">Proteins</span>
        </div>
        
        <div class="panel-body">
          
          <div class="form-group">
            <label class="col-lg-4 control-label">Protein A ID (4 letters code) from www.pdb.org</label>
            <div class="col-lg-1">
              <input type="text" name="protein1" id="protein1" size=5 maxlength=4 onblur="checkID(this);" value="2bxs">
            </div>
            
            <label class="col-lg-2 control-label">or upload</label>
            <div class="col-lg-3">
              <input type="file" name="protein1UP" id="protein1UP" onchange="validateFileExtension(this);">
            </div>
          </div>
          <br>
          <div class="form-group">
            <label class="col-lg-4 control-label">Protein B ID (4 letters code) from www.pdb.org</label>
            <div class="col-lg-1">
              <input type="text" name="protein2" id="protein2" size=5 maxlength=4 onblur="checkID(this);">
            </div>
            <label class="col-lg-2 control-label">or upload</label>
            <div class="col-lg-3">
              <input type="file" name="protein2UP" id="protein2UP" onchange="validateFileExtension(this);"> <a href="<?php echo $GLOBALS["Host"] . "/SERT_SWISSMODEL.pdb"; ?>">(Example: SERT_SWISSMODEL.pdb)</a>
            </div>
          </div>
          
        </div>
        
        <div class="panel-footer clearfix">
          <div class="pull-right">
            <button class="btn btn-primary" OnClick='validateProteinsIDs();'>Process files</button>
            <button class="btn btn-secondary" OnClick='resetForm();'>Reset</button>
          </div>
        </div>
        
      </div>
      
    </div>
  </div>
  
  <!--</form>-->
  <form onsubmit="return validateParams()" name="formParams" id="formParams" method="POST" action="compareProtein.php" enctype="multipart/form-data" class="form-horizontal">
  <!-- row -->
  <div class="row">
    
    <!-- column -->
    <div class="col-lg-12">
      
      <div class="panel panel-default">
        
        <div id="spinner"></div>
        
        <div class="panel-heading">
          <span class="panel-title">Parameters</span>
        </div>
        
        <div id="collapse1" class="panel-collapse collapse on">
        <div class="panel-body" id="panelBodyParams">
          
          <div class="form-group">
            <label class="col-lg-2 control-label">Chain Protein A</label>
            <div class="col-lg-4">
              <select class="col-lg-4" style="text-align: right;" name="chainProtA" id="chainProtA">
              </select>
              &nbsp;&nbsp;&nbsp;&nbsp;<img src="imgs/help2.png" title="Chains in the Protein A">
            </div>
            <label class="col-lg-2 control-label">Chain Protein B</label>
            <div class="col-lg-4">
              <select class="col-lg-4" style="text-align: right;" name="chainProtB" id="chainProtB">
              </select>
              &nbsp;&nbsp;&nbsp;&nbsp;<img src="imgs/help2.png" title="Chains in the Protein B">
            </div> 
          </div>
          
          <hr>
          
          <div class="form-group">
            <label class="col-lg-2 control-label">Near Threshold (Nt)</label>
            <div class="col-lg-4">
              <input style="text-align: right;" type="number" name="uMin" id="uMin" value="5"> &#197; <img src="imgs/help2.png" title="To form a 3D pattern, each residue must be at least &#197; away from any other.">
            </div>
            <label class="col-lg-2 control-label">Far Threshold (Ft)</label>
            <div class="col-lg-4">
             <input style="text-align: right;" type="number" name="uMax" id="uMax" size=4 maxlength=3 value="10"> &#197;  <img src="imgs/help2.png" title="To form a 3D pattern, each residue must be at most &#197; away from any other. For instance, if we are searching for cavities associated with the binding of a metal ion, Nt and Ft must be much smaller than those set when larger ligands, such as ATP or catecholamine neurotransmitters are investigated.">
            </div> 
          </div>

  <!-- 
          <div class="form-group">
            <label class="col-lg-2 control-label">Size Threshold (St)</label>
            <div class="col-lg-4">
              <input style="text-align: right;" type="number" name="minResidue" id="minResidue" size=4 maxlength=3 value="20"> &nbsp;&nbsp; <img src="imgs/help2.png" title="To form a 3D pattern, the number of residues considered  must be set. For the sake of flexibility St - 3 and St + 3 may be taken as limiting values.">
            </div>
            <label class="col-lg-2 control-label">Volume Threshold (Vt)</label>
            <div class="col-lg-4">
              <input style="text-align: right;" type="number" name="minVolume" id="minVolume" size=4 maxlength=4 value="750"> &#197;&sup3;<img src="imgs/help2.png" title="To form a 3D pattern, the volume formed by all residues considered must be at least &#197;&sup3;.">
            </div>
          </div>
    
  -->       
          
          
          <hr>

          <div class="form-group">
            <label class="col-lg-2 control-label">SDist</label>
            <div class="col-lg-4">
              <input style="text-align: right;" type="number" name="distancePCT" id="distancePCT" size=4 maxlength=4 value="25">&nbsp;&nbsp;&nbsp;&nbsp;<img src="imgs/help2.png" title="Relative contribution of distance feature. This is a percentage value.">
            </div>
            <label class="col-lg-2 control-label">SNbE</label>
            <div class="col-lg-4">
              <input style="text-align: right;" type="number" name="nbondedPCT" id="nbondedPCT" size=4 maxlength=4 value="25">&nbsp;&nbsp;&nbsp;&nbsp;<img src="imgs/help2.png" title="Relative contribution of non-bonded energy feature. This is a percentage value.">
            </div>
          </div>
           
          <div class="form-group">
            <label class="col-lg-2 control-label">STsp</label>
            <div class="col-lg-4">
              <input style="text-align: right;" type="number" name="tspPCT" id="tspPCT" size=4 maxlength=4 value="25">&nbsp;&nbsp;&nbsp;&nbsp;<img src="imgs/help2.png" title="Relative contribution of the perimeter feature. This is a percentage value.">
            </div>
            <label class="col-lg-2 control-label">SSc</label>
            <div class="col-lg-4">
              <input style="text-align: right;" type="number" name="assPCT" id="assPCT" size=4 maxlength=4 value="25">&nbsp;&nbsp;&nbsp;&nbsp;<img src="imgs/help2.png" title="Relative contribution of the sequence component feature. This is a percentage value. The user has the option of setting different values if, for instance,  a specific property seems to be more important for a certain analysis (i.e. if SSc = SNbE = 50, only the sequence component and the Non-bonding energy descriptors are weighted in the final score).">
            </div>
          </div>

          <hr>
            
          <div class="form-group">
            <label class="col-lg-2 control-label">Grid Spacing (GS)</label>
            <div class="col-lg-4">
              <input style="text-align: right;" type="number" name="gridPCT" id="gridPCT" size=4 maxlength=3  value="5">&#197 <img src="imgs/help2.png" title="This value is the size of squares that composed the searching grid." >
            </div> 
            <label class="col-lg-2 control-label">Filter Similar</label>
            <div class="col-lg-4">
              <input style="text-align: right;" type="number" name="filterMax" id="filterMax" size=4 maxlength=3  value="50">% <img src="imgs/help2.png" title="This value is a percentage and represent the threshold of similarities. If the user set this value in 85%, just the pair of 3D-patterns with a final similarity score higher than 85% will be shown.">
            </div>
          </div>

          <hr>
            
          <div class="form-group">
            <!-- <label class="col-lg-2 control-label"><input type="checkbox" name="sendMail" id="sendMail"> Send results to</label>-->
            <label class="col-lg-2 control-label">Send results to</label>
            <div class="col-lg-4">
              <input type="email"  placeholder="email" name="emailDest" id="emailDest" size=50 maxlength=50 required>
              <input type="hidden" name="Idprotein1" id="Idprotein1">
              <input type="hidden" name="Idprotein2" id="Idprotein2">
              <input type="hidden" name="tmpPDBPath" id="tmpPDBPath">
            </div>
          </div>
            
        </div>
        
        <div class="panel-footer clearfix">
          <div class="pull-right">
            <button class="btn btn-primary"  disabled="disabled" id="btnCmp" name="btnCmp">Compare proteins</button>
          </div>
        </div>
        </div>
        
      </div>
      
    </div>
    
  </div>
  </form>
    
</div> <!-- /container -->

<?php
foot_page();
close_body_page();
?>

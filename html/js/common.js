//http://wiki.jmol.org/index.php/Jmol_JavaScript_Object/Info
/* opciones para el applet de JMOL */
Info = {
  use: "<set below>",
  width: "<set below>",
  height: "<set below>",
  debug: false,
  color: "black",
  j2sPath: "j2s",
  jarPath: "java",
  disableJ2SLoadMonitor: true,
  disableInitialConsole: true,
  addSelectionOptions: false,
  allowjavascript: true,
  script: "<set below>"
}

/* opciones para el spinner */
var opts = {
  lines: 13, // The number of lines to draw
  length: 20, // The length of each line
  width: 10, // The line thickness
  radius: 30, // The radius of the inner circle
  corners: 1, // Corner roundness (0..1)
  rotate: 0, // The rotation offset
  color: '#000', // #rgb or #rrggbb
  speed: 1, // Rounds per second
  trail: 60, // Afterglow percentage
  shadow: false, // Whether to render a shadow
  hwaccel: true, // Whether to use hardware acceleration
  className: 'spinner', // The CSS class to assign to the spinner
  zIndex: 2e9, // The z-index (defaults to 2000000000)
  top: '20%', // Top position relative to parent in px
  left: '50%' // Left position relative to parent in px
};

/* applet JMOL para pockets */
function getAppletPockets(pdbPocket, eldiv, elapplet) {
  Info.use = "HTML5";
  Info.width = 500;
  Info.height = 500; 
  if (pdbPocket != 'empty') {
    Info.script = "background [245,245,245];";
    Info.script = Info.script + "set zoomLarge false; load " + pdbPocket +";";
    Info.script = Info.script + " select *.CA; label \"%n%r\";color black;";
  }
  
  $(eldiv).html(Jmol.getAppletHtml(elapplet, Info))
}

/* applet JMOL para proteinas con el pocket adentro */
function getAppletProteinWithPockets(chain, pdbfile, aminos, eldiv, elapplet) {
  Info.use = "HTML5";
  Info.width = 500;
  Info.height = 500;
  var res = aminos.split(",");
   
  if (pdbfile != 'empty') {
    Info.script = "background [245,245,245];";
    Info.script = Info.script + "load files \"" + pdbfile + "\";";
    //Info.script = Info.script + "select ligand;";
    //Info.script = Info.script + "color red;spacefill;lcaocartoon scale 4.0  CAP unitcell \"cpk\";spacefill off;";
    //Info.script = Info.script + "select "+ res[0]+"; center selected; select*;";
    //Info.script = Info.script + "hide *;display *:" + chain + "; cpk off;rockets on;color rockets structure;ribbons off;cartoons off;";
    Info.script = Info.script + "hide *;" + chain + "; cpk off;rockets on;color rockets structure;ribbons off;cartoons off;";
    //Info.script = Info.script + "hide *; cpk off;rockets on;color rockets structure;ribbons off;cartoons off;";
    Info.script = Info.script + "select ligand;";
    Info.script = Info.script + "color blue; spacefill;";
    Info.script = Info.script + "select " + aminos + "; center selected;";
    Info.script = Info.script + "color green; spacefill;";
    //Info.script = Info.script + "color green;spacefill;lcaocartoon scale 1.0  CAP unitcell \"cpk\";spacefill off;cpk off";
    //Info.script = Info.script + "select ligand;";
    //Info.script = Info.script + "color red;spacefill;lcaocartoon scale 4.0  CAP unitcell \"cpk\";spacefill off;";
  }
   
  $(eldiv).html(Jmol.getAppletHtml(elapplet, Info))
}

/* muestra proteinas con pocket adentro */
function showProteinWithPocket(chain1, chain2, pdb1, pdb2, pock1, pock2) {
  p1sr = pock1.concat(".txt");
  p2sr = pock2.concat(".txt");
  
  var data2 ="";
  var data3 ="";
  
  $.post(p1sr)
    .done(function( data ) {
    data2 = data;
    getAppletProteinWithPockets(chain1, pdb1, data2, "#mydiv3", "jmolApplet3");
  });

  $.post(p2sr)
    .done(function( data ) {
    data3 = data;
    getAppletProteinWithPockets(chain2, pdb2, data3, "#mydiv4", "jmolApplet4");
  });
}

function showPockets(pdb1, pdb2, pock1, pock2) {
  getAppletPockets(pock1,"#mydiv1","jmolApplet0");
  getAppletPockets(pock2,"#mydiv2","jmolApplet1");
}

/* llama a las funciones para mostrar los applets JMOL */
function show3D(chain1, chain2, pdb1, pdb2, pock1, pock2) {
  var title1 = pock1.split('/');
  var title2 = pock2.split('/');
  title1 = title1[title1.length - 1].split('.')[0];
  title2 = title2[title2.length - 1].split('.')[0];
  
  var title3 = pdb1.split('.')[0];
  var title4 = pdb2.split('.')[0];
  
  $("#titlejsmol1").html("[" + title3 + "<a href='" + pdb1 + "'> <img src='imgs/pdb.png' title='Download PDB file'></a>] [" + title1 + " <a href='" + pock1 + "'> <img src='imgs/pdb.png' title='Download PDB file'></a>]");
  
  $("#titlejsmol2").html("[" + title4 + "<a href='" + pdb2 + "'> <img src='imgs/pdb.png' title='Download PDB file'></a>] [" + title2 + " <a href='" + pock2 + "'> <img src='imgs/pdb.png' title='Download PDB file'></a>]");
  
  showPockets(pdb1, pdb2, pock1, pock2);
  
  if (chain1=='empty') {
    chain1 = "display *";
  } else {
    chain1 = "display *:" + chain1;
  }
  
  if (chain2=='empty') {
    chain2 = "display *";
  } else {
    chain2 = "display *:" + chain2;
  }
  
  showProteinWithPocket(chain1, chain2, pdb1, pdb2, pock1, pock2);
}

/* verifica en RCSB si ID existe */
function checkID (input) {
  if (input.value.length > 0) {
    
    $.ajax({
      type: "GET",
      url: 'https://www.rcsb.org/pdb/rest/describePDB',
      data: {structureId: input.value},
      dataType: 'xml',
      success: function (response) {
        if ($(response).find("PDB").length == 0) {
          alert("Protein ID: " + input.value + " not exist");
          input.value = "";
        }
      }
    });
  }
}

/* valida extensión .pdb */
function validateFileExtension(oInput) {
  var sFileName = oInput.value;
  if (sFileName.length > 0) {
    var sCurExtension = ".pdb";
    
    if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
      return true;
    } else {
      alert("Sorry, " + sFileName + " is invalid, allowed extension is: .pdb");
      oInput.value = "";
      return false;
    }
  }
}

/* valida IDs de proteinas */
function validateProteinsIDs() {  
  var Idprotein1 = "";
  var Idprotein2 = "";
  
  if ($('#protein1').val().length<1 && $('#protein1UP').val().length<1) {
    alert("Missing Protein A");
    return false;
  }
  
  if ($('#protein2').val().length<1 && $('#protein2UP').val().length<1) {
    alert("Missing Protein B");
    return false;
  }
  
  if ($('#protein1').val().length>0 && $('#protein1UP').val().length>0) {
    alert("Please either upload a PDB file OR enter a PDB ID for  protein A.");
    return false;
  }
  
  if ($('#protein2').val().length>0 && $('#protein2UP').val().length>0) {
    alert("Please either upload a PDB file OR enter a PDB ID for  protein B.");
    return false;
  }
  
  var protein1UP;
  if ($('#protein1UP').prop('files')[0]) {
    protein1UP  = $('#protein1UP').prop('files')[0];
    Idprotein1 = protein1UP.name.split(".")[0]
  } else {
    protein1UP = "";
  }
  
  var protein2UP;
  if ($('#protein2UP').prop('files')[0]) {
   protein2UP = $('#protein2UP').prop('files')[0];
   Idprotein2 = protein2UP.name.split(".")[0]
  } else {
    protein2UP = "";
  }
  
  if ($('#protein1').val().length>0) {
    Idprotein1 = $('#protein1').val();
  }
  
  if ($('#protein2').val().length>0) {
    Idprotein2 = $('#protein2').val();
  }
  
  var protein1 = $('#protein1').val();
  var protein2 = $('#protein2').val();
  
  // crea formulario temporal.
  var form_data = new FormData();
  // agrega objetos al formulario temporal.
  form_data.append('protein1UP', protein1UP);
  form_data.append('protein2UP', protein2UP);
  form_data.append('protein1', protein1);
  form_data.append('protein2', protein2);
  
  var target = document.getElementById('spinner');
  var spinner = new Spinner(opts).spin(target);
  
  $.ajax({
    type: 'POST',
    url: 'getPDBs.php',
    dataType: 'json',  // what to expect back from the PHP script, if anything
    async: true,
    data: form_data,
    cache: false,
    contentType: false,
    processData: false,
    success: function(response) {
      obj = JSON.parse(response);
      
      options = "<select>";
      for (var k in obj.proteinA) {
        options = options + "<option value='" + obj.proteinA[k] + "'>" + obj.proteinA[k] + "</option>";
      }
      options = options + "</select>";
      $("#chainProtA").html(options);
      
      options = "<select>";
      for (var l in obj.proteinB) {
        options = options + "<option value='" + obj.proteinB[l] + "'>" + obj.proteinB[l] + "</option>";
      }
      options = options + "</select>";
      $("#chainProtB").html(options);
      
      // coloca ID de las proteínas en el formluario de parámetros.
      $('#Idprotein1').val(Idprotein1);
      $('#Idprotein2').val(Idprotein2);
      $('#tmpPDBPath').val(obj.tmpPDBPath);
      
      $("#btnCmp").prop('disabled', false);
      
      $('#collapse1').collapse('show');
      spinner.stop();
    },
  });
  
  
  
  return true;
}

function resetForm() {
  $('#protein1').val("");
  $('#protein2').val("");
  $('#protein1UP').val("");
  $('#protein2UP').val("");
  $("#btnCmp").prop('disabled', true);
  
  document.getElementById("chainProtA").innerHTML = "";
  document.getElementById("chainProtB").innerHTML = "";
}

/* valida los parámetros de entrada para la comparación */
function validateParams() {
  if ($('#uMin').val().length<1) {
    alert("Missing Near Threshold (Nt)");
    return false;
  }
  
  if ($('#uMax').val().length<1) {
    alert("Missing Far Threshold (Ft)");
    return false;
  }
  
  /*
  if ($('#minResidue').val().length<1) {
    alert("Missing Size Threshold (St)");
    return false;
  }
  */
  
  /*
  if ($('#minVolume').val().length<1) {
    alert("Missing Volume Threshold (Vt)");
    return false;
  }
  */
  
  //if ($('#chargePCT').val().length<1) {
  if ($('#nbondedPCT').val().length<1) {
    //alert("Missing SCh");
    alert("Missing SNbE");
    return false;
  }
  
  if ($('#distancePCT').val().length<1) {
    alert("Missing SDist");
    return false;
  }
  
  if ($('#tspPCT').val().length<1) {
    alert("Missing STsp");
    return false;
  }
  
  if ($('#assPCT').val().length<1) {
    alert("Missing SSaa");
    return false;
  }
  
  //if (parseInt($('#chargePCT').val()) + parseInt($('#distancePCT').val()) + parseInt($('#tspPCT').val()) + parseInt($('#assPCT').val()) !== 100) {
  if (parseInt($('#nbondedPCT').val()) + parseInt($('#distancePCT').val()) + parseInt($('#tspPCT').val()) + parseInt($('#assPCT').val()) !== 100) {
    alert("The sum of relative contributions of SDist, SCh, STsp and SSaa must be 100%");
    return false;
  }
  
  /*
  if ($('#filterMin').val().length<1) {
    alert("Missing Filter Differences");
    return false;
  }
  */
  
  if ($('#filterMax').val().length<1) {
    alert("Missing Filter Similarities");
    return false;
  }

  if ($('#gridPCT').val().length<1) {
    alert("Missing Grid Size");
    return false;
  }
  
  /*
  if ($('#sendMail').is(':checked')) {
    if ($('#emailDest').val().length<1) {
      alert("Missing email");
      return false;
    }
  }*/

  if ($('#emailDest').val().length<1) {
    alert("Missing email");
    return false;
  }
  
  alert("The link with the results will be sent to your email. Thank you for using Geomfinder.");

  var target = document.getElementById('spinner');
  var spinner = new Spinner(opts).spin(target);
  
  return true;
}

/* muestra el grafo */
/*
function showVIS() {
  var nodes = null;
  var edges = null;
  var network = null;

  $.getJSON('dats/results.json', function(mydata) {
    nodes = mydata.nodes;
    edges = mydata.edges;
    
    // Instantiate our network object.
    var container = document.getElementById('mygraph');
    var data = {
      nodes: nodes,
      edges: edges
    };
    var options = {
      stabilize: false,
      smoothCurves: false,
      navigation: false,
      keyboard: false,
      dataManipulation: false,
      hover: true,
      nodes: {
        shape: 'dot',
      },
      edges: {
        color: 'black',
      },
      groups: {
        protein1: {
          color: '#FFFF10',
          fontColor: 'black',
        },
        protein2: {
          color: '#808080',
          fontColor: 'black',
        }
      },
    };
    network = new vis.Network(container, data, options);
  });

}
*/

/* obtiene los scores JSON */
function getScores() {
    $.getJSON('dats/scores.json', function(mydata) {

    $('#table-scores').dataTable({
          "aaData": mydata.scores,
          "columnDefs": [
            { "title": "JMOL <img src='imgs/help2.png' title='View 3D-Pattern'>", "targets": 0, "searchable": false, "mDataProp": "url", "orderable": false},
            { "title": "3D-pattern A <img src='imgs/help2.png' title='3D-pattern identified in protein A'> ", "targets": 1, "searchable": true, "mDataProp": "pocket1" },
            { "title": "3D-pattern B <img src='imgs/help2.png' title='3D-pattern identified in protein B'> ", "targets": 2, "searchable": true, "mDataProp": "pocket2" },
            { "title": "**GScore** <img src='imgs/help2.png' title='Final similarity score (GScore). GScore is a combination of the partial similarities of the Charge (SCh), Distance (SDist), Perimeter (STsp) and Solvent Accessible Area (Saa). This value is a pertentage of similarities ( 0 to 100)'> ", "targets": 3, "searchable": false, "mDataProp": "scoref" },
            { "title": "SNbE <img src='imgs/help2.png' title='Partial similarity score of the non-bonded energy feature between the 3D-patterns A and B'> ", "targets": 4, "searchable": false, "mDataProp": "scorenbondedf" },
            { "title": "SDist <img src='imgs/help2.png' title='Partial similarity score of the Distance feature between the 3D-patterns A and B'> ", "targets": 5, "searchable": false, "mDataProp": "scoredistf" },
            { "title": "STsp <img src='imgs/help2.png' title='Partial similarity score of the Perimeter  between the 3D-patterns A and B'> ", "targets": 6, "searchable": false, "mDataProp": "scoretspf" },
            { "title": "SSc <img src='imgs/help2.png' title='Partial similarity score of sequence component between the 3D-patterns A and B'> ", "targets": 7, "searchable": false, "mDataProp": "scoreASSf" },
            /*{ "title": "Vol A &#197;&sup3;<img src='imgs/help2.png' title='Volume of the 3D-pattern A'> ", "targets": 8, "searchable": false, "mDataProp": "volP1" },
            { "title": "Vol B &#197;&sup3;<img src='imgs/help2.png' title='Volume of the 3D-pattern B'> ", "targets": 9, "searchable": false, "mDataProp": "volP2" },*/
            { "title": "Size A <img src='imgs/help2.png' title='Number of residues of the 3D-pattern A'> ", "targets": 8, "searchable": false, "mDataProp": "resP1" },
            { "title": "Size B <img src='imgs/help2.png' title='Number of residues of the 3D-pattern B'> ", "targets": 9, "searchable": false, "mDataProp": "resP2" },
            { "title": "Known BS A <img src='imgs/help2.png' title='Coincidences with a known binding site'> ", "targets": 10, "searchable": false, "mDataProp": "Bsite1" },
            { "title": "Known BS B <img src='imgs/help2.png' title='Coincidences with a known binding site'> ", "targets": 11, "searchable": false, "mDataProp": "Bsite2" },
          ],
          "bDestroy": true,
          "scrollX": true,
          "text-align": true,
          "order": [[ 3, "desc" ]],
          "searching": true,
          "search": {
            "regex": true,
            "smart": false
          }
        });
  });
}

/* obtiene los parámetros JSON */
function getParams() {
   $.getJSON('dats/params.json', function(mydata) {
     $("#protein1").html(mydata.params[0].protein1);
     $("#protein2").html(mydata.params[0].protein2);
     $("#umbralMin").html(mydata.params[0].umbralMin + "&#197;");
     $("#umbralMax").html(mydata.params[0].umbralMax + "&#197;");
     //$("#lenPCT").html(mydata.params[0].lenPCT);
     //$("#volPCT").html(mydata.params[0].volPCT + "&#197;&sup3;");
     $("#nbondedPCT").html(mydata.params[0].nbondedPCT + "%");
     $("#distancePCT").html(mydata.params[0].distancePCT + "%");
     $("#tspPCT").html(mydata.params[0].tspPCT + "%");
     $("#assPCT").html(mydata.params[0].assPCT + "%");
     $("#filterMax").html(mydata.params[0].filterMax + "%");
     $("#chainProtA").html(mydata.params[0].chainP1);
     $("#chainProtB").html(mydata.params[0].chainP2);
     $("#gridPCT").html(mydata.params[0].gridPCT+ "&#197;");
   });
}

function getProteinsName() {
   $.getJSON('dats/params.json', function(mydata) {
     $("#p1").html('<img src="imgs/protein1.png"> 3D-Patterns of ' + mydata.params[0].protein1 + '<br>');
     $("#p2").html('<img src="imgs/protein2.png"> 3D-Patterns of ' + mydata.params[0].protein2);
   });
}

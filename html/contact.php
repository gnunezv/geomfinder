<?php
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  untitled.py
#  
#  Copyright 2015 gabriel Nunez <gabriel@ganunez>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

require_once 'lib/common.php';
head_page();
open_body_page();
?>

<p></p>
<!--<div class="container theme-showcase" role="main">-->
<div class="container-fluid">
  <div class="page-header">
    <h2><?php echo $GLOBALS["Name"]; ?>: <?php echo $GLOBALS["Title"]; ?> </h2>
  </div>
  
  <div class="row">
    
    <!-- column -->
    <div class="col-lg-12">
     <div class="panel panel-default">
        <div class="panel-heading center">
          <span class="panel-title">Contact Us</span>
        </div>
        
        <div class="panel-body">
          <div class="text-justify">
          If you have any question about <?php echo $GLOBALS["Name"]; ?>, please send us an email to <a href="mailto: geomfinder@gmail.com">geomfinder@gmail.com</a> or <a href="mailto: ganunez@utalca.cl">ganunez@utalca.cl</a>
          </div>
        </div>
      </div>
      
    </div>
  </div>

</div>

</div> <!-- /container -->


<?php
foot_page();
close_body_page();
?>


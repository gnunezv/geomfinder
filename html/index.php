<?php
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  untitled.py
#  
#  Copyright 2015 gabriel Nunez <gabriel@ganunez>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

require_once 'lib/common.php';
head_page();
open_body_page();
?>

<!--<div class="container theme-showcase" role="main">-->
<div class="container-fluid">
  <div class="page-header">
    <h2><?php echo $GLOBALS["Name"]; ?>: <?php echo $GLOBALS["Title"]; ?> </h2>
  </div>
  
  <!-- row -->
  <div class="row">
    <!-- column -->
    <div class="col-lg-7">
      <div class="panel panel-default">
        <div class="panel-heading center">
          <span class="panel-title">Resume</span>
        </div>
        
        <div class="panel-body">
          <div class="text-justify">
          <?php echo $GLOBALS["Name"]; ?> is an intuitive, flexible and ligand-independent web server for detailed estimation of similarities between 
          all pairs of 3D patterns detected in any two given protein structures. We tested <?php echo $GLOBALS["Name"]; ?> in almost 1.100 proteins, 
          detecting 3D patterns and measuring their structural similarity in different subsets of these proteins. Thus: <p>
          <ol type=a>
            <li> <?php echo $GLOBALS["Name"]; ?> detected identical pairs of 3D patterns in a series of monoamine oxidase-B structures, which 
            corresponded to the effectively similar ligand binding sites at these proteins;</li> 
            <li> we identified structural similarities among pairs of protein structures which are targets of compounds such 
            as acarbose, benzamidine, adenosine triphosphate and pyridoxal phosphate; these similar 3D patterns are not detected 
            using sequence-based methods;</li> 
            <li> the detailed evaluation of three specific cases showed the versatility of <?php echo $GLOBALS["Name"]; ?>, which was able to 
            discriminate between similar and different 3D patterns related to binding sites of common substrates in a range of 
            diverse proteins.
          </ol>

          If you found <?php echo $GLOBALS["Name"]; ?> helpful, please cite: <i>Gabriel Nuñez-Vivanco, Alejandro Valdés-Jiménez, F. B. M. R.-P. 
          Geomfinder: a multi-feature identifier of similar three-dimensional protein patterns: a ligand-independent approach. Journal 
          of Cheminformatics (2016). <a href="https://jcheminf.springeropen.com/articles/10.1186/s13321-016-0131-9" target="_blank">https://jcheminf.springeropen.com/articles/10.1186/s13321-016-0131-9</a></i>
          </div>
        </div>

        <div class="panel-footer clearfix">
          <div class="pull-right">
            <a href="run.php" class="btn btn-primary center center-block" role="button">Submit a Job</a>
          </div>
        </div>
      </div>

    </div>
  <!--</div> -->


  <!--<div class="row"> -->
    <!--<div class="col-lg-2"></div>-->
    
    <div class="col-lg-5">
      <div class="panel panel-default">
        <div class="panel-heading center">
          <span class="panel-title">Example Screencast</span>
        </div>
        
        <div class="panel-body">
        <div class="embed-responsive embed-responsive-16by9">
          <iframe class="embed-responsive-item" frameborder="0" 
            src="https://www.youtube.com/embed/saWU82Z3CU8?rel=0&amp;showinfo=0" 
            allowfullscreen=""></iframe>
        </div>
        </div>
      </div>
    </div>
    
  <!--</div> -->

</div> <!-- /container -->

<?php
foot_page();
close_body_page();
?>


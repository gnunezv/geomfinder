<?php
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  untitled.py
#  
#  Copyright 2015 gabriel Nunez <gabriel@ganunez>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
$tmpPDBPath =  "results/" . microtime(true);
exec("mkdir " . $tmpPDBPath, $res);
  
$p1OK = FALSE;
$p2OK = FALSE;
    
# p1 subir?
$finalProtein1 = "";
if (strlen($_FILES['protein1UP']['name'])>0) {
  $p1 = $_FILES['protein1UP']['name'];
  $uploadfile1 = $tmpPDBPath . "/" . basename($_FILES['protein1UP']['name']);
  $finalProtein1 = $uploadfile1;
  
  if (move_uploaded_file($_FILES['protein1UP']['tmp_name'], $uploadfile1)) {
    $p1OK = TRUE;
  } else {
    echo "error! upload Protein A";
  }
# p1 descargar?
} else {
  if (strlen($_POST['protein1'])>0) {
    $p1 = $_POST['protein1'].".pdb";
    $dw1 = "wget -q http://www.rcsb.org/pdb/files/" . $p1 . " -O " . $tmpPDBPath . "/" . $p1;
    exec($dw1, $down1);
    $finalProtein1 = $tmpPDBPath . "/" . $p1;
    $p1OK = TRUE;
  }####FIXME: y si falla la descarga?????
}
  
### protein1 ok?
if ($p1OK) {
  # p2 subir?
  $finalProtein2 = "";
  if (strlen($_FILES['protein2UP']['name'])>0) {
    $p2 = $_FILES['protein2UP']['name'];
    $uploadfile2 = $tmpPDBPath . "/" . basename($_FILES['protein2UP']['name']);
    $finalProtein2 = $uploadfile2;
    
    if (move_uploaded_file($_FILES['protein2UP']['tmp_name'], $uploadfile2)) {
      $p2OK = TRUE;
    } else {
      echo "error! upload Protein B";
    }
    # p2 descargar?
  } else {
    if (strlen($_POST['protein2'])>0) {
      $p2 = $_POST['protein2'].".pdb";
      $dw2 = "wget -q http://www.rcsb.org/pdb/files/" . $p2 . " -O " . $tmpPDBPath . "/" . $p2;
      exec($dw2, $down2);
      $finalProtein2 = $tmpPDBPath . "/" . $p2;
      $p2OK = TRUE;
    } 
  }
  
  if ($p2OK) {
    ############## OK """"""""""""""
    $cmd = "python -W ignore getChains.py " . $tmpPDBPath . " " . $finalProtein1 . " " . $finalProtein2;
    exec($cmd, $results);
    echo json_encode($results[0]);
  }
} else {
  echo "error!";
}
?>

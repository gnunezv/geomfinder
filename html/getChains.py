#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  untitled.py
#  
#  Copyright 2015 gabriel Nunez <gabriel@ganunez>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

from Bio.PDB import PDBIO
from Bio.PDB.PDBParser import PDBParser
import ujson as json
import sys

def getChains(pdbfile, jsonData, protein):
  # lee estructura del PDB.
  parser = PDBParser()
  structure = parser.get_structure(pdbfile, pdbfile)
    
  # siempre considerar el primer modelo.
  model = structure[0]

  # FIXME: que pasa con las cadenas ' ' ?
  allChain = []
  for chain in structure.get_chains():
    if chain.get_parent().get_id() == 0:
      if chain.get_id() == ' ':
        allChain.append('empty')
      else:
        allChain.append(chain.get_id())
        
        ###if not chain.get_id() in allChain:
          ###allChain.append(chain.get_id())

  jsonData[protein] = allChain

def main():
  jsonData = {}
  getChains(sys.argv[2], jsonData, "proteinA")
  getChains(sys.argv[3], jsonData, "proteinB")
  jsonData["tmpPDBPath"] = sys.argv[1]
 
  print json.dumps(jsonData)
  return 0

if __name__ == '__main__':
  main()


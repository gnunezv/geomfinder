<?php
$Name = "Geomfinder";
$Version = "v1.0";
$Title = "A multi-feature identifier of similar tridimensional protein patterns. A ligand-independent approach.";
$Host = "http://appsbio.utalca.cl/geomfinder/";

/*
 * encabezado de página
 */
function foot_page(){
?>
<div class="text-center">
  <a href="http://icb.utalca.cl/">School of Bioinformatics Engineering</a> | Department of Bioinformatics | <a href="http://www.utalca.cl/">Universidad de Talca</a> | Talca, Chile
</div>
<?php
}

function head_page(){
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><?php echo $GLOBALS["Name"] . " " . $GLOBALS["Version"]; ?></title>
  <!-- ############### JS ############## -->
  <!-- jquery http://code.jquery.com/jquery/ -->
  <!--<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-2.1.3.min.js"></script>-->
  <script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>
  <!-- jsmol -->
  <script type="text/javascript" src="jmol/jsmol/JSmol.min.js"></script>
  <!-- jquerydataTable -->
  <!--<script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js"></script>-->
  <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
  <!-- spin.js loader -->
  <script src="js/spin.min.js"></script>
  <!-- Bootstrap Core JavaScript -->
  <!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>-->
  <script src="js/bootstrap.min.js"></script>
  <!-- custom -->
  <script type="text/javascript" src="js/common.js"></script>
  
  <!-- ############### CSS ############## -->
  <!-- bootstrap -->
  <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">-->
  <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
  <!-- dataTables -->
  <!--<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.5/css/jquery.dataTables.css">-->
  <link rel="stylesheet" type="text/css" href="css/jquery.dataTables.css">
  <!-- custom CSS -->
  <link rel="stylesheet" type="text/css" href="css/estilo.css">
</head>
<?php
}

/*
 * parte inicial de la página
 */
function open_body_page() {
?>
<!--<body role="document">-->
<body>
<!--<nav class="navbar navbar-inverse navbar-fixed-top">-->
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.php"><?php echo $GLOBALS["Name"]; ?></a>
   </div>
      
    <div id="navbar" class="collapse navbar-collapse">
      <ul class="nav navbar-nav">
        <li><a href="<?php echo $GLOBALS["Host"]; ?>index.php">Home</a></li>
        <li><a href="<?php echo $GLOBALS["Host"]; ?>run.php">Submit a Job</a></li>
        <li><a href="<?php echo $GLOBALS["Host"]; ?>example.pdf" target="_blank">Example</a></li>
        <li><a href="<?php echo $GLOBALS["Host"]; ?>contact.php">Contact Us</a></li>
      </ul>
    </div><!--/.nav-collapse -->
  </div>
</nav>
<?php
}

/*
 * parte final de la página
 */
function close_body_page() {
?> 
</body>
</html>
<?php
}
?>

<?php
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  untitled.py
#  
#  Copyright 2015 gabriel Nunez <gabriel@ganunez>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
 
require_once 'lib/common.php';
head_page();
open_body_page();
require 'PHPMailerAutoload.php';

?>
<div class="container-fluid">
  <div class="page-header">
    <h2><?php echo $GLOBALS["Name"]; ?>: <?php echo $GLOBALS["Title"]; ?> </h2>
  </div>

<?php
  # concatena comando a ejecutar.
  $cmd = "python -W ignore ../geomfinder.py ";
  $cmd = $cmd . $_POST['tmpPDBPath'] . "/" . $_POST['Idprotein1'] . ".pdb " . $_POST['tmpPDBPath'] . "/" . $_POST['Idprotein2'] . ".pdb";
  $cmd = $cmd . " -thresholdMin " . $_POST['uMin'];
  $cmd = $cmd . " -thresholdMax " . $_POST['uMax'];
  $cmd = $cmd . " -nbondedPCT " . $_POST['nbondedPCT'];
  $cmd = $cmd . " -distancePCT " . $_POST['distancePCT'];
  $cmd = $cmd . " -tspPCT " . $_POST['tspPCT'];
  $cmd = $cmd . " -assPCT " . $_POST['assPCT'];
  $cmd = $cmd . " -filterMax " . $_POST['filterMax'];
  $cmd = $cmd . " -chainP1 " . $_POST['chainProtA'];
  $cmd = $cmd . " -chainP2 " . $_POST['chainProtB'];
  $cmd = $cmd . " -gridPCT " . $_POST['gridPCT'];
  
  
  ###print_r($_POST);
  ###print ($cmd);
  # ejecuta la búsqueda.
  exec($cmd, $pathData);
  
  # arrojó resultados?
  if (strcmp($pathData[0], "None") == 0) {
    ?>
    <div class="row">
      <!-- column -->
      <div class="col-lg-12">
        <div class="panel panel-warning">
          <div class="panel-heading">
            <span class="panel-title"></span>
          </div>
          <div class="panel-body" align="center">
            <h3>No similar 3d patterns were found between <?php echo $_POST['Idprotein1'] ?> and <?php echo $_POST['Idprotein2'] ?>. You can modify the parameters of the <?php echo $GLOBALS["Name"]; ?> and then run again.</h3>
          </div>
        </div>
      </div>
    </div>
    <!-- row -->
    <div class="row">
      <!-- column -->
      <div class="col-lg-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            <span class="panel-title">Parameters used</span>
          </div>
          <div class="panel-body">

            Protein A: <label id="protein1"><?php echo $_POST['Idprotein1']; ?></label> Chain: <label id="chainProtA"><?php echo $_POST['chainProtA']; ?></label> Near Threshold: <label id="umbralMin"><?php echo $_POST['uMin']; ?>&#197;</label> STsp: <label id="tspPCT"><?php echo $_POST['tspPCT']; ?>%</label> SNbE: <label id="nbondedPCT"><?php echo $_POST['nbondedPCT']; ?>%</label> Grid Spacing: <label id="gridPCT"><?php echo $_POST['gridPCT']; ?>&#197;</label>

            <br>
            Protein B: <label id="protein2"><?php echo $_POST['Idprotein2']; ?></label> Chain: <label id="chainProtB"><?php echo $_POST['chainProtB']; ?></label> Far Threshold: <label id="umbralMax"><?php echo $_POST['uMax']; ?>&#197;</label> SDist: <label id="distancePCT"><?php echo $_POST['distancePCT']; ?>%</label> SSc: <label id="assPCT"><?php echo $_POST['assPCT']; ?>%</label> Filter Similarities: <label id="filterMax"><?php echo $_POST['filterMax']; ?>%</label>

          </div>
          
        </div>
      </div>
    </div>
    
    <!--
    <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            <span class="panel-title">Help</span>
          </div>
          
          <div class="panel-body">
          ...
          </div>
          
        </div>
      </div>
    </div>
    -->
    
    <script type="text/javascript">
      getParams();
    </script>
    <?php
    
  } else {
    # mover los 2 archivos PDBs temporales a carpeta de resultados.
    $move = "mv " . $_POST['tmpPDBPath'] . "/" . $_POST['Idprotein1'] . ".pdb " . " " . $pathData[0];
    exec($move);
    $move = "mv " . $_POST['tmpPDBPath'] . "/" . $_POST['Idprotein2'] . ".pdb " . " " . $pathData[0];
    exec($move);
  
    $dir = $pathData[0] . "/pngs";
    $dirDats = $pathData[0] . "/dats";
    
    ###$copyIndex = "cp indexTemplate.php " . $pathData[0] . "/index.php";
    ###exec($copyIndex);
        
    # elimina directorio temporal de descarga de PDBs.
    $move = "rm -rf " . $_POST['tmpPDBPath'];
    exec($move);
        
    # cambia de directorio
    chdir($pathData[0]);
        
    # crea enlaces simbólicos necesarios.
    exec("ln -s ../../indexTemplate.php index.php");
    exec("ln -s ../../css/ css");
    exec("ln -s ../../imgs/ imgs");
    exec("ln -s ../../lib/ lib");
    exec("ln -s ../../js/ js");
    exec("ln -s ../../jmol/ jmol");
    exec("ln -s ../../j2s/ j2s");
    ###exec("ln -s ../../graph.php .");
        
    # concatena URL.
    $urlResults = $GLOBALS["Host"] . $pathData[0] . "/index.php?p1=" . $_POST['Idprotein1'] . "&p2=" . $_POST['Idprotein2'] . "&tmpPDBPath=" . $_POST['tmpPDBPath'];
        
    # enviar notificación por correo?.
    ##if ($_POST['sendMail']) {
    sendMailNotification($_POST['emailDest'], $urlResults);
    ##}
        
    # redirecciona a los resultados
    header("Location: " . $urlResults);
  }
?>

</div> <!-- /container -->

<?php
close_body_page();

function sendMailNotification($to, $url) {
  $mail = new PHPMailer;
  $mail -> CharSet = "UTF-8";
  $mail->isSMTP();
  $mail->SMTPDebug = 0;
  
  $mail->Host = 'smtp.gmail.com';
  $mail->Port = 587;
  $mail->SMTPSecure = 'tls';
  $mail->SMTPAuth = true;
  $mail->Username = 'geomfinder@gmail.com';
  $mail->Password = 'geomfinderv1';
  $mail->addBCC('ganunez@utalca.cl');

  $mail->From = 'geomfinder@gmail.com';
  $mail->FromName = $GLOBALS["Name"];
  // Add a recipient
  $mail->addAddress($to);
  // Add a recipient

  $mail->SMTPOptions = array(
    'ssl' => array(
      'verify_peer' => false,
      'verify_peer_name' => false,
      'allow_self_signed' => true
    )
  );

  // Set email format to HTML
  $mail->isHTML(true);

  $mail->Subject = $GLOBALS["Name"] . ' results';
  $mail->Body    = 'Dear user:<BR>Thanks for using the ' . $GLOBALS["Name"] . ' server.<BR>The results of your query can be seen in the following link:' . '<a href="' . $url . '"> View results</a>'.'<BR>The link will be available by 60 days from today<P> <B>' . $GLOBALS["Name"] . ' team';
  $mail->AltBody = 'View results: ' .$url;

  /*
  if(!$mail->send()) {
    echo 'Mailer Error: ' . $mail->ErrorInfo;
  } else {
    echo 'Message has been sent';
  }*/
}
?>

<?php
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  untitled.py
#  
#  Copyright 2015 gabriel Nunez <gabriel@ganunez>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
 
require_once '../../lib/common.php';
head_page();
open_body_page();

?>
<!--<div class="container theme-showcase" role="main">-->
<div class="container-fluid">
  <div class="page-header">
    <h2><?php echo $GLOBALS["Name"]; ?>: <?php echo $GLOBALS["Title"]; ?> </h2>
  </div>

  <?php
  showPage($_GET["p1"], $_GET["p2"], $_GET["tmpPDBPath"]);
  ?>

</div> <!-- /container -->

<script type="text/javascript">
  getParams();
  getScores();
</script>

<?php
close_body_page();

function showPage($p1, $p2, $tmpPDBPath) {
?>   
    <!-- row -->
    <div class="row">
      <!-- column -->
      <div class="col-lg-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            <span class="panel-title"><a data-toggle="collapse" href="#collapse1">Parameters used</a></span>
          </div>
          
          <div id="collapse1" class="panel-collapse collapse in">
          <div class="panel-body">
            Protein A: <label id="protein1"></label> Chain: <label id="chainProtA"></label> Near Threshold: <label id="umbralMin"></label> STsp: <label id="tspPCT"></label> SNbE: <label id="nbondedPCT"></label> Grid Spacing: <label id="gridPCT"></label>
            <br>
            Protein B: <label id="protein2"></label> Chain: <label id="chainProtB"></label> Far Threshold: <label id="umbralMax"></label> SDist: <label id="distancePCT"></label> SSc: <label id="assPCT"></label> Filter Similarities: <label id="filterMax"></label>
          </div>
          </div>
          
        </div>
      </div>
    </div>
    
    <!-- row -->
    <div class="row">
      <!-- column -->
      <div class="col-lg-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            <!--<span class="panel-title">[View Graph of Similarities <a href="graph.php" target="_blank"><img src="imgs/jmol.png"></a>]</span> -->
            <span class="panel-title">Results</span>
          </div>
          
          <div class="panel-body">
            <div class="table" id="listScores">
              <table id="table-scores" class="display compact" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
          
        </div>
      </div>
    </div>
    
    <!-- row -->
    <div class="row">
      <!-- column -->
      <div class="col-lg-6">
        <div class="panel panel-default">
          <div class="panel-heading">
            <span class="panel-title" id="titlejsmol1">Protein A</span>
          </div>
          
          <div class="panel-body" align="center">
            <div align="center" id="mydiv1"></div>
          </div>
          
          <div class="panel-body" align="center">
            <div align="center" id="mydiv3"></div>
          </div>
          
        </div>
      </div>
      
      <!-- column -->
      <div class="col-lg-6">
        <div class="panel panel-default">
          <div class="panel-heading">
            <span class="panel-title" id="titlejsmol2">Protein B</span>
          </div>
          
          <div class="panel-body" align="center">
            <div align="center" id="mydiv2"></div>
          </div>
          
          <div class="panel-body" align="center">
            <div align="center" id="mydiv4"></div>
          </div>
          
        </div>
      </div>
    </div>
    <?php
}
?>

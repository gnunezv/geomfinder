# To install Geomfinder in a Linux-based OS, it must accomplish the following dependencies:

+ tsp-solver: https://github.com/dmishin/tsp-solver
  + git clone https://github.com/dmishin/tsp-solver.git
  + install as root: python setup.py install
+ python 2.7
+ python-biopython
+ python-ujson

# To execute Geomfinder through command line, use the following instructions:


python -Wignore geomfinder.py 1EXP.pdb 2O3P.pdb

+ The results will be available in:

results/

+ To configure in your computer/server the web-based access to  Geomfinder, follow the instructions detailed in the html/README file.

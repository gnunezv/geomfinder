#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  pdbPocket.py
#  
#  Copyright 2015 Gabriel Nuñez-Vivanco <ganunez@utalca.cl>, Alejandro Valdés-Jiménez <avaldes@utalca.cl> 
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  

from Bio.PDB import PDBIO
from Bio.PDB.Structure import Structure 
from Bio.PDB.Model import Model 
from Bio.PDB.Chain import Chain 
from Bio.PDB.Residue import Residue
from Bio.PDB.Atom import Atom

class PDBPOCKET(object):
  def __init__(self, filename):
    self.filename = filename
    self.s = Structure (0)
    self.m = Model(0)
    self.s.add(self.m)
    self.chain = Chain("A")
    self.m.add(self.chain)
  
  def createResidue(self, field, resseq, icode):
    r = Residue(field, resseq, icode)
    return r
  
  def createAtom(self, name, coord, b_factor, occupancy, altloc, fullname, serial_number=None, element=None):
    a = Atom (name, coord, b_factor, occupancy, altloc, fullname, serial_number, element)
    return a
  
  def addResidueToChain(self, residue):
    self.chain.add(residue)
  
  def addAtomToResidue(self, residue, atom):
    residue.add(atom)

  def writePDBFile(self):
    io = PDBIO()
    io.set_structure(self.s)
    io.save(self.filename +".pdb")

'''
pp = PDBPOCKET("aaaa")
r = pp.createResidue((' ', 100, ' '), 'MET', 0)
pp.addResidueToChain(r)
a = pp.createAtom('CA',(11,22,33), 0, 1, ' ', ' CA', 0, 'C')
pp.addAtomToResidue(r, a)
pp.writePDBFile()
'''
